let input = document.getElementById('price');


input.onfocus = () => input.style.borderColor = "green";

let div = document.createElement('div');
let span = document.createElement('span');
let deleteBtn = document.createElement('button');

let warning = document.createElement('p');
warning.innerText = "Please enter correct price";


deleteButton = () => {
    deleteBtn.onclick = () => {
        input.value = "";
        input.style.backgroundColor = "";
        deleteBtn.parentElement.remove();
    }
}


input.onchange = () => {
	if(+input.value >= 0){
		warning.remove();
        span.innerText = `Current Price: ${input.value}`;
        
        div.appendChild(span);
        
        deleteBtn.innerText = 'X';
        
		deleteButton();
        
        div.appendChild(deleteBtn);
        

        document.body.insertBefore(div, document.body.children[1]);
        
        input.style.backgroundColor = 'green';
        
	}else{

       
        
        input.style.backgroundColor = "red";
        
        document.body.appendChild(warning);
        
		
	}
}
