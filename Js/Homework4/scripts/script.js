function createNewUser() {
    let name = prompt("Please enter your name");
    while(!name) name = prompt("Please enter a valid  name");
    let surname = prompt("Please enter your surname");
    while(!surname) surname = prompt("Please enter a valid surname");
   
    const newUser = {
        firstName : name,
        lastName : surname,
        get getFirstName(){ return this.firstName},
        set setFirstName(value){ this.firstName = value },
        get getLastName (){ return this.lastName},
        set setLastName(value) { this.lastName = value },
        getLogin : function() { return this.getFirstName.charAt(0).toLowerCase() + this.getLastName.toLowerCase()}
    };
    return newUser;
}

const newUser = createNewUser();
console.log(newUser.getLogin());

