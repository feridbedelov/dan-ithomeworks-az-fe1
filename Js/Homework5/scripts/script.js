function createNewUser() {
    let name = prompt("Please enter your name");
    while(!name) name = prompt("Please enter a valid  name");
    let surname = prompt("Please enter your surname");
    while(!surname) surname = prompt("Please enter a valid surname");
    let birthDate = prompt("Please enter your birthday in dd.mm.yyyy format ");
    while(!birthDate) birthDate = prompt("Please enter a valid birthday in dd.mm.yyyy format");
   
    let birthdayArray = birthDate.split('.');


    const newUser = {
        firstName : name,
        lastName : surname,
        birthday : new Date(birthdayArray[2],birthdayArray[1],birthdayArray[0]),
        get getFirstName(){ return this.firstName},
        set setFirstName(value){ this.firstName = value },
        get getLastName (){ return this.lastName},
        set setLastName(value) { this.lastName = value },
        getAge : function() {return new Date().getFullYear() - this.birthday.getFullYear()},
        getPassowrd : function() {
            return this.getFirstName.charAt(0).toUpperCase() + this.getLastName.toLowerCase()+this.birthday.getFullYear()
        },
        getLogin : function() { return this.getFirstName.charAt(0).toLowerCase() + this.getLastName.toLowerCase()}

    };
    return newUser;
}

const newUser = createNewUser();
console.log(newUser.getPassowrd());
console.log(newUser.getAge());
