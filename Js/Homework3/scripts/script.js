function calc(a,b,operation){
    switch (operation) {
        case '+':
            return a+b;
            break;
        case '-':
            return a-b;
            break;
        case '*':
            return a*b;
            break;
        case '/':
            if (b===0) {
                return "Cant be divided by zero";
            }
            else{
                return a/b;
            }
            break;
    
        default:
            return "There is no operation like that";
            break;
    }
}


let number1 = prompt("Enter the first number:");
while(isNaN(number1)) number1 = prompt("Please enter a valid number");
let number2 = prompt("Enter the second number");
while(isNaN(number2)) number2 = prompt("Please enter a valid number");

let num1 = parseFloat(number1);
let num2 = parseFloat(number2);


let op = prompt("Please enter valid operator(+,-,*,/)");
while(op !== "+" && op !== "-" && op !== "*" && op !== "/")
    op =  prompt("Please enter operator(+,-,*,/)");


console.log(calc(num1,num2,op));


