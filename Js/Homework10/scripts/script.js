let items = document.querySelectorAll('i.icon-password');

for (let i = 0; i < items.length; i++) {
    
    let currentIcon = items[i];

    currentIcon.onclick = () => {

        if (currentIcon.classList.contains('fa-eye')) {
           currentIcon.previousElementSibling.type = 'text';
           currentIcon.classList.remove('fa-eye');            
           currentIcon.classList.add('fa-eye-slash');
        } else {
            currentIcon.previousElementSibling.type='password';
            currentIcon.classList.remove('fa-eye-slash');
            currentIcon.classList.add('fa-eye');
        }


    }
    
}

let passInput1 = document.getElementById('pass1');
let passInput2 = document.getElementById('pass2');

let btn = document.getElementById('btn-s');

let p = document.createElement('p');
p.innerText = "You need to enter the identical values";
p.style.color = 'red';


btn.onclick = () =>{
	if (passInput1.value === passInput2.value ) {
		p.remove();
		alert('You are welcome');
	} else {
		
		document.body.insertBefore(p,document.body.children[1]);
	}
}