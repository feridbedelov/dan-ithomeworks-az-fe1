let style = document.getElementById('theme');

if (localStorage.theme === "alt") {
	style.href = "css/altStyle.css";
}

let changingCss = document.getElementById("changeTheme");

changingCss.onclick = () => {
	if (localStorage.theme === "alt") {
		localStorage.theme = "";
		style.href = "css/style.css";
	}else{ 
		localStorage.theme = "alt";
		style.href = "css/altStyle.css";
	}
}
