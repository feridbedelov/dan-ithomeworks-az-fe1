let wrapper = document.getElementById('imageWrapper');

for (let i = 0; i < wrapper.children.length; i++){
	wrapper.children[i].dataset.index = i;
	if(i !== 0)
    wrapper.children[i].hidden = true;
}

let intervalId, timerIntervalId;

function startInterval(){
    timer.innerText = "10.00";
	timerIntervalId = setInterval(() => {
		timer.innerText = (timer.innerText - 0.01).toFixed(2);
	}, 10)

	intervalId = setInterval(() => {
		timer.innerText = "10.00";
		let current = wrapper.querySelector('img:not([hidden])');
		current.hidden = true;
		if (current === wrapper.lastElementChild){
			wrapper.firstElementChild.hidden = false;
		}else{
			wrapper.children[+current.dataset.index + 1].hidden = false;
		}
    }, 10000)
   
};

let stop = document.createElement('button');
let resume = document.createElement('button');
let timer = document.createElement('p');

stop.innerText = "Stop";
stop.style.backgroundColor ='red';
resume.innerText = "Resume";
resume.style.backgroundColor ='green';
resume.disabled = true;

stop.onclick = () => {
	clearInterval(timerIntervalId);
	clearInterval(intervalId);
	resume.disabled = false;
	stop.disabled = true;
}
resume.onclick = () => {
	startInterval();
	resume.disabled = true;
	stop.disabled = false;
}

document.body.appendChild(timer);
document.body.appendChild(stop);
document.body.appendChild(resume);

startInterval();

