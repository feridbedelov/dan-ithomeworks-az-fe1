createList = (array) => {
    
    let ul = document.createElement("ul");

    array.map(function(item){
        let li = document.createElement("li");
        
        if(typeof item === "object"){
            let childUl = document.createElement("ul");
            Object.entries(item).forEach(function(objectItems){
                let childLi = document.createElement("li");
                childLi.innerHTML = `<a href='#'>${objectItems[0]+":"+objectItems[1]}</a>`;
                childUl.appendChild(childLi);
            });
            li.appendChild(childUl);
        }
        
        else if(Array.isArray(item)){
            let childUl = document.createElement("ul");
            item.map(function(subItem){
                let childLi = document.createElement("li");
                childLi.innerHTML = `<a href='#'>${subItem}</a>`;
                childUl.appendChild(childLi);
            });
            li.appendChild(childUl);
        }
        
        else{
            li.innerHTML =  `<a href='#' > ${item} </a>`;
        }
        ul.appendChild(li);

    });
    document.body.appendChild(ul);
}


// createList(["a","b",["c1","c2","c3","c4"],1,2,3,4, {time:"Now", value: 999}]);

createList([12,'a','v',[1,5,5],'name',4,78,['a','b','v'],{name:'ferid',surname:'bedelov'}]);